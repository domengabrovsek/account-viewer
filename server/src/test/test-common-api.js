'use strict';

const { expect } = require('chai');
const initServer = require('../index');

describe('Common API tests', function () {

  let app, request;

  before(async function () {
    app = await initServer();
    request = require('supertest')(app);
  });

  it('GET /status', async function () {

    const response = await request.get('/status');
    expect(response.status).to.equal(200);
  });
});

