'use strict';

const { expect } = require('chai');
const initServer = require('../index');

describe('Stellar API tests', function () {

  let app, request;

  before(async function () {
    app = await initServer();
    request = require('supertest')(app);
  });

  it('GET /stellar/transactions', async function () {

    const response = await request
      .get('/stellar/transactions')
      .query({ id: process.env.XLM_ACCOUNT_ID });

    expect(response.status).to.equal(200);
    expect(response.body).to.exist;
    expect(response.body).to.be.instanceOf(Array);
    expect(response.body.length).to.be.above(0);
  });

  it('GET /stellar/operations', async function () {

    const response = await request
      .get('/stellar/operations')
      .query({ id: process.env.XLM_ACCOUNT_ID });

    expect(response.status).to.equal(200);
    expect(response.body).to.exist;
    expect(response.body).to.be.instanceOf(Array);
    expect(response.body.length).to.be.above(0);
  });

  it('GET /stellar/payments', async function () {

    const response = await request
      .get('/stellar/payments')
      .query({ id: process.env.XLM_ACCOUNT_ID });

    expect(response.status).to.equal(200);
    expect(response.body).to.exist;
    expect(response.body).to.be.instanceOf(Array);
    expect(response.body.length).to.be.above(0);
  });

  it('GET /stellar/account', async function () {

    const response = await request
      .get('/stellar/account')
      .query({ id: process.env.XLM_ACCOUNT_ID });

    expect(response.status).to.equal(200);
    expect(response.body).to.exist;
    expect(response.body).to.be.instanceOf(Object);
    expect(response.body.id).to.exist;
    expect(response.body.balance).to.exist;
  });
});

