'use strict';

const initServer = require('./index');
const Logger = require('./loaders/logger');

initServer()
  .then(app => {
    const port = process.env.PORT;
    app.listen(port, err => {
      if (err) {
        Logger.error(err);
        throw new Error(err);
      }

      Logger.info(`Server listening on port: ${port}`);
    });
  })
  .catch(error => {
    Logger.error(error);
  });
