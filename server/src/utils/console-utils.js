
'use strict';

const util = require('util');

// helper to output nested objects in console (to avoid [Object] object output)
const logNestedObj = (obj) => obj && console.log(util.inspect(obj, false, null, true)); // eslint-disable-line no-console

module.exports = {
  logNestedObj
};
