'use strict';

const Sequelize = require('sequelize');
const isDocker = require('is-docker');

const mysqlOptions = {
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  host: isDocker() ? process.env.DOCKER_DB_HOST : process.env.DB_HOST
};

const sequelize = new Sequelize(
  process.env.DB_NAME,
  process.env.DB_USER,
  process.env.DB_PASSWORD, {
    host: process.env.DB_HOST,
    dialect: process.env.DIALECT,
    logging: false,
    freezeTableName: true
  });

module.exports = {
  mysqlOptions,
  sequelize,
  Sequelize
};
