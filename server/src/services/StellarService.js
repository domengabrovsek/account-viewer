'use strict';

const StellarSdk = require('stellar-sdk');
const Server = new StellarSdk.Server('https://horizon.stellar.org');

const Logger = require('../loaders/logger');

module.exports = class StellarService {

  async GetAccountInfo(accountId) {
    Logger.info(`Fetching info for account ${accountId}`);
    const result = await Server
      .accounts()
      .accountId(accountId)
      .call();

    return {
      id: result.account_id,
      inflationDestination: result.inflation_destination,
      balance: result.balances[0].balance
    };
  }

  async GetOperations(accountId) {

    Logger.info(`Fetching operations for account ${accountId}`);

    const getFirstCursor = async (accountId) =>
      (await Server.operations().forAccount(accountId).limit(1).call()).records[0].id;

    const operations = [];
    let totalResults = 0;
    let iteration = 1;
    let cursor = await getFirstCursor(accountId);

    // iterate over pages of operations and get them all
    while (true) {

      // for debugging purposes
      Logger.info(`Current cursor: ${cursor}`);

      // get page of results
      const results = (await Server
        .operations()
        .forAccount(accountId)
        .limit(100)
        .cursor(cursor)
        .call()).records;

      // number of records in current page
      const noOfRecords = results.length;
      totalResults += noOfRecords;
      Logger.info(`${iteration}.iteration - results: ${noOfRecords}`);

      // push current page of results to total result
      results.forEach(record => operations.push(record));

      // we're at the end of results
      if (results.length === 0) {
        break;
      }

      // get next cursor
      cursor = results[noOfRecords - 1]._links.succeeds.href.match(/[\d]{17,20}/).toString();

      iteration++;
    }

    Logger.info(`Total results fetched: ${totalResults}`);

    return operations.map(o => ({
      id: o.id,
      successful: o.transaction_successful,
      sourceAccount: o.source_account || o.from,
      destinationAccount: o.to,
      amount: o.amount,
      type: o.type,
      timestamp: o.created_at,
      transactionId: o.transaction_hash
    }));
  }

  async GetPayments(accountId) {

    const promises = [this.GetTransactions(accountId), this.GetOperations(accountId)];
    const [transactions, operations] = await Promise.allSettled(promises);

    const payments = operations.value
      .filter(o => o.type === 'payment')
      .map(o => ({
        ...o,
        memo: transactions.value.find(t => t.id === o.transactionId).memo
      }));

    return payments;
  }

  async GetTransactions(accountId) {

    Logger.info(`Fetching transactions for account ${accountId}`);

    const getFirstCursor = async (accountId) =>
      (await Server.transactions().forAccount(accountId).limit(1).call()).records[0]._links.succeeds.href.match(/[\d]{17,20}/).toString();

    const transactions = [];
    let totalResults = 0;
    let iteration = 1;
    let cursor = await getFirstCursor(accountId);

    // iterate over pages of transactions and get them all
    while (true) {

      // for debugging purposes
      Logger.info(`Current cursor: ${cursor}`);

      // get page of results
      const results = (await Server
        .transactions()
        .forAccount(accountId)
        .limit(100)
        .cursor(cursor)
        .call()).records;

      // number of records in current page
      const noOfRecords = results.length;
      totalResults += noOfRecords;
      Logger.info(`${iteration}.iteration - results: ${noOfRecords}`);

      // push current page of results to total result
      results.forEach(record => transactions.push(record));

      // we're at the end of results
      if (results.length === 0) {
        break;
      }

      // get next cursor
      cursor = results[noOfRecords - 1]._links.succeeds.href.match(/[\d]{17,20}/).toString();

      iteration++;
    }

    Logger.info(`Total results fetched: ${totalResults}`);

    return transactions.map(t => ({
      id: t.id,
      memo: t.memo || 'Empty memo'
    }));
  }
};
