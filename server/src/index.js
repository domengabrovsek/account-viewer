'use strict';

async function initServer() {

  const express = require('express');
  const app = express();

  // load env variables
  require('dotenv').config();

  // load on startup
  await require('./loaders/index')({ app });

  return app;
}

module.exports = initServer;
