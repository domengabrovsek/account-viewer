'use strict';

const { createLogger, format, transports, config } = require('winston');
const { combine, timestamp, label, printf, colorize } = format;

const myFormat = printf(({ level, message, label, timestamp }) => {
  return `${timestamp} [${label}] ${level}: ${message}`;
});

const LoggerInstance = createLogger({
  // winston logging (error, warn, info, http, verbose, debug, silly)
  level: 'silly',
  levels: config.npm.levels,

  format: combine(
    label({ label: 'acc-viewer' }),
    timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
    colorize(),
    myFormat,
  ),
  transports: [new transports.Console()]
});

module.exports = LoggerInstance;
