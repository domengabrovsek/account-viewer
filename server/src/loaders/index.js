'use strict';

const expressLoader = require('./express');
const dbLoader = require('./db');
const Logger = require('./logger');

module.exports = ({ app }) => {

  // TODO: uncomment this when database will actually be used
  // await dbLoader();
  // Logger.info('Database loaded!');

  expressLoader({ app });
  Logger.info('Express loaded!');
};
