'use strict';

const cors = require('cors');
const bodyParser = require('body-parser');

const commonRouter = require('../routes/common');
const krakenRouter = require('../routes/kraken');
const stellarRouter = require('../routes/stellar');

module.exports = ({ app }) => {

  // Enable Cross Origin Resource Sharing to all origins by default
  app.use(cors());

  // Middleware that transforms the raw string of req.body into json
  app.use(bodyParser.json());

  // register routers
  app.use(commonRouter);
  // app.use(krakenRouter); // commented out until kraken router is refactored in kraken.js
  app.use(stellarRouter);

  // catch 404 and forward to error handler
  app.use((req, res, next) => {
    const err = new Error('Not Found');
    err['status'] = 404;
    next(err);
  });

  // error handlers
  app.use((err, req, res, next) => {
    /**
     * Handle 401 thrown by express-jwt library
     */
    if (err.name === 'UnauthorizedError') {
      return res
        .status(err.status)
        .send({ message: err.message })
        .end();
    }
    return next(err);
  });

  app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.json({
      errors: {
        message: err.message,
      },
    });
  });
};
