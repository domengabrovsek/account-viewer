'use strict';

const router = new (require('express')).Router();
const Logger = require('../loaders/logger');

// status test endpoint
router.get('/status', (req, res) => {
  try {
    const start = process.hrtime();

    const end = process.hrtime(start);
    Logger.info(`Processed request. Took ${end[0]}s, ${end[1] / 1000000} ms`);

    res.status(200).send({ status: 'OK', message: 'Server is working!' });
  } catch (error) {
    Logger.error(error);
    res.status(500).send(error);
  }
});

module.exports = router;
