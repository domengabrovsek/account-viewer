'use strict';

const router = new (require('express')).Router();
const StellarService = new (require('../services/StellarService'))();
const Logger = require('../loaders/logger');

router.get('/stellar/transactions', async (req, res, next) => {

  try {
    const start = process.hrtime();
    const accountId = req.query.id;
    Logger.info(`Received request.`);

    const result = await StellarService.GetTransactions(accountId);

    const end = process.hrtime(start);

    Logger.info(`Processed request. Took ${end[0]}s, ${end[1] / 1000000} ms`);
    res.send(result);

  } catch (error) {
    Logger.error(error);
    res.status(500).send(error);
  }
});

router.get('/stellar/operations', async (req, res, next) => {

  try {
    const start = process.hrtime();
    const accountId = req.query.id;
    Logger.info(`Received request.`);

    const result = await StellarService.GetOperations(accountId);

    const end = process.hrtime(start);

    Logger.info(`Processed request. Took ${end[0]}s, ${end[1] / 1000000} ms`);
    res.send(result);

  } catch (error) {
    Logger.error(error);
    res.status(500).send(error);
  }
});

router.get('/stellar/payments', async (req, res, next) => {

  try {
    const start = process.hrtime();
    const accountId = req.query.id;
    Logger.info(`Received request.`);

    const result = await StellarService.GetPayments(accountId);

    const end = process.hrtime(start);

    Logger.info(`Processed request. Took ${end[0]}s, ${end[1] / 1000000} ms`);
    res.send(result);

  } catch (error) {
    Logger.error(error);
    res.status(500).send(error);
  }
});

router.get('/stellar/account', async (req, res, next) => {

  try {
    const start = process.hrtime();
    const accountId = req.query.id;
    Logger.info(`Received request.`);

    const result = await StellarService.GetAccountInfo(accountId);

    const end = process.hrtime(start);

    Logger.info(`Processed request. Took ${end[0]}s, ${end[1] / 1000000} ms`);
    res.send(result);

  } catch (error) {
    Logger.error(error);
    res.status(500).send();
  }

});

module.exports = router;
