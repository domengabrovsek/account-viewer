# Crypto account viewer
Learning and playing with different technologies trying to build account viewer for various cryptocurrencies.

## Prerequisites
- docker
- node js version >= 12.9.0

## Get started

### Install dependencies

```js 
// server
~ cd server
~ npm install

// client
~ cd client
~ npm install

```

### Run the app
```js

// server
~ cd server
~ npm run start -> mysql db will start on port 3306 and express server will start on port 3000

// client
~ cd client
~ npm run serve

```

## npm commands
```js
// run tests
npm run test

// run linter
npm run lint

// run linter autofix
npm run lint-fix
```


## currently supported endpoints

### Common
```js
// server status
/status
```

### Stellar (XLM)
```js

// get all transactions for specified account
/stellar/transactions?id=[your_account_id]

// get all operations for specified account
/stellar/operations?id=[your_account_id]

// get all payments for specified account
/stellar/payments?id=[your_account_id]

// get account info for specified account
/stellar/account?id=[your_account_id]

```
